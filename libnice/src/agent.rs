use crate::{Agent, Candidate};
use glib::translate::*;
use std::ptr;

impl Agent {
    #[doc(alias = "nice_agent_get_selected_pair")]
    pub fn get_selected_pair(
        &self,
        stream_id: u32,
        component_id: u32,
    ) -> Option<(Candidate, Candidate)> {
        unsafe {
            let mut local = ptr::null_mut();
            let mut remote = ptr::null_mut();
            let ret = from_glib(ffi::nice_agent_get_selected_pair(
                self.to_glib_none().0,
                stream_id,
                component_id,
                &mut local,
                &mut remote,
            ));
            if ret {
                Some((from_glib_full(local), from_glib_full(remote)))
            } else {
                None
            }
        }
    }
}
