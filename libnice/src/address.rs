use crate::Address;
use glib::translate::ToGlibPtrMut;
use std::net::{Ipv4Addr, Ipv6Addr};

impl Address {
    /// Set `self` to an IPv4 address using the data from `addr_ipv4`
    ///
    ///  `<note>`
    ///  `<para>`
    ///  This function will reset the port to 0, so make sure you call it before
    ///  [`set_port()`][Self::set_port()]
    ///  `</para>`
    ///  `</note>`
    /// ## `addr_ipv4`
    /// The IPv4 address
    #[doc(alias = "nice_address_set_ipv4")]
    pub fn set_ipv4(&mut self, addr: &Ipv4Addr) {
        unsafe {
            ffi::nice_address_set_ipv4(self.to_glib_none_mut().0, (*addr).into());
        }
    }

    /// Set `self` to an IPv6 address using the data from `addr_ipv6`
    ///
    ///  `<note>`
    ///  `<para>`
    ///  This function will reset the port to 0, so make sure you call it before
    ///  [`set_port()`][Self::set_port()]
    ///  `</para>`
    ///  `</note>`
    /// ## `addr_ipv6`
    /// The IPv6 address
    #[doc(alias = "nice_address_set_ipv6")]
    pub fn set_ipv6(&mut self, addr: &Ipv6Addr) {
        unsafe {
            ffi::nice_address_set_ipv6(self.to_glib_none_mut().0, &addr.octets() as *const u8);
        }
    }

    /// Set the port of `self` to `port`
    /// ## `port`
    /// The port to set
    #[doc(alias = "nice_address_set_port")]
    pub fn set_port(&mut self, port: u16) {
        unsafe {
            ffi::nice_address_set_port(self.to_glib_none_mut().0, port as u32);
        }
    }
}
